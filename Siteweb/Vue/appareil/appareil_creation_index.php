<!DOCTYPE html>
<html lang="fr">

<head>
    <meta CHARSET="UTF-8">
    <link rel="icon" href="../../img/economy.jpg" type="image/x-icon" />
    <title>Inscription</title>
    <link rel="stylesheet" href="../../Vue/style.css" type="text/css">
    <script src='../../Controleur/script.js'></script>
</head>

<body>
    <div id="dialogoverlay"></div>
    <div id="dialogbox">
        <div>
            <div id="dialogboxhead"></div>
            <div id="dialogboxbody">
            </div>
            <div id="dialogboxfoot"></div>
        </div>
    </div>
    <input type="image" class="return" src="../../img/return.png" onclick="history.replaceState(null,null,'../../Vue/utilisateur/utilisateur_principale_index.php');location.reload();">
    <h1 class="title">Créer un appareil</h1>
    <table class="login">
        <form method="post" >
            <tr>
                <td>
                    <label for="EmplacementAppareil">Emplacement Appareil</label>
                </td>
                <td>
                    <input type="text" name="EmplacementAppareil" id="EmplacementAppareil" >
                </td>
                <td>
                    <span id="Libelle_appareil_label"></span>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="typeappareil">Type Appareil</label>
                </td>
                <td>
                    <select name="typeappareil" id="region-select">
                        <?php 
                            include("../../Controleur/appareil/ajax_typeappareil.php");
                            $liste = ListeTypeAppareils();
                            foreach ($liste as $value) {
                                echo "
                                <option value=\"".$value["IdTypeAppareil"]."\">".$value["LibelleTA"]."</option>
                                ";
                            }
                            
                        ?>
                    </select>
                </td>
                <td>
                    <span id="idappareil_label"></span>
                </td>
            </tr>
    </table>
    <input type="submit" value="Valider" name="submit" id="submit">
    </form>
    <?php include "../../Controleur/appareil/appareil_creation_data.php" ?>
</body>

</html>