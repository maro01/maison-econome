<?php
session_start();
if (!isset($_SESSION['login_success']) || !$_SESSION['login_success']) {
	echo "<script>location.replace('../../Controleur/utilisateur/connexion_deconnexion.php');</script>";
}
?>
<!DOCTYPE html>
<html>

<head>
	<meta CHARSET="UTF-8">
	<link rel="icon" href="../../img/economy.jpg" type="image/x-icon" />
	<title>Accueil</title>
	<link rel="stylesheet" href="../style.css" type="text/css">
    <script src='../../Controleur/script.js'></script>
</head>

<body>
	<div id="dialogoverlay"></div>
	<div id="dialogbox">
		<div>
			<div id="dialogboxhead"></div>
			<div id="dialogboxbody">
			</div>
			<div id="dialogboxfoot"></div>
		</div>
	</div>
	
	<input type="image" class="return" src="../../img/return.png" onclick="history.replaceState(null,null,'../utilisateur/utilisateur_principale_index.php');location.reload();">
		<h1 style='	
    color:white;
	font-family:tahoma;
	font-size:25px;
	text-decoration:underline;
	text-align:center;
	text-shadow: 
	3px 3px 0 black,
    -1px -1px 0 black,  
     1px -1px 0 black,
    -1px 1px 0 black,
     1px 1px 0 black;'
	 >Pièces<br><br></h1>
<?php
	 	if (isset($_REQUEST["ajout"])) {
		echo "<h4 style='color:rgb(63,169,95);font-family:tahoma;		text-shadow: 
	1px 1px 0 black,
    -1px -1px 0 black,  
     1px -1px 0 black,
    -1px 1px 0 black,
     1px 1px 0 black;'>Ajout réussi</h4>";
	}
	 	if (isset($_REQUEST["supp"])) {
		echo "<h4 style='color:rgb(63,169,95);font-family:tahoma;		text-shadow: 
	1px 1px 0 black,
    -1px -1px 0 black,  
     1px -1px 0 black,
    -1px 1px 0 black,
     1px 1px 0 black;'>Suppression réussie</h4>";
	}
		 	if (isset($_REQUEST["err"])) {
		echo "<h4 style='color:rgb(255,0,0);font-family:tahoma;		text-shadow: 
	1px 1px 0 black,
    -1px -1px 0 black,  
     1px -1px 0 black,
    -1px 1px 0 black,
     1px 1px 0 black;'>Erreur</h4>";
	}
?>
	<form method="post" action="">

		<table class="menu">
			<tr class="menu">

				<th class="menu">Nom de l'appareil</th>
				<th class="menu">Libelle de l'appareil</th>
                <th class="menu">Emplacement dans la pièce</th>
				<th class="menu">Supprimer</th>
			</tr>
			<?php
				include("../../Controleur/appareil/ajax_typeappareil.php");
				$liste = ListeAppareilsPiece();
				foreach ($liste as $value) {
					echo "
					<tr class=\"menu\">
						<td class=\"menu\">".$value["NomTA"]."</td>
						<td class=\"menu\">".$value["LibelleTA"]."</td>
                        <td class=\"menu\">".$value["EmplacementAppareil"]."</td>
						<td class=\"menu\"><a class=\"cc\" href=\"#\" onclick='Alert.delete_appareil(".$_GET['IdPiece'].",".$value['IdAppareil'].");'>Supprimer</a></td>
					</tr>
					";
				}
			?>
		</table>
	</form>
	<?php echo "<br><br><a class='button' href='appareil_creation_index.php?IdPiece=".$_GET['IdPiece']."'>Ajouter un appareil</a>"; ?>
</body>
</html>