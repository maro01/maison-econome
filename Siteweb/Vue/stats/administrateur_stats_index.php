<?php
session_start();
if (!isset($_SESSION['login_success']) || !$_SESSION['login_success']) {
	echo "<script>location.replace('../../Controleur/utilisateur/connexion_deconnexion.php');</script>";
}

include('../../Modele/connexion_bd.php');
?>
<!DOCTYPE html>
<html>

<head>
    <meta CHARSET="UTF-8">
    <link rel="icon" href="../../img/economy.jpg" type="image/x-icon" />
    <title>Statistiques</title>
    <link rel="stylesheet" href="../style.css" type="text/css">
    <script src="../../package_chart_js/dist/Chart.js"></script> <!-- Import de Chart.js -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> <!-- Import de JQuery -->
    <script> // Change certaines valeurs par défaut pour les graphiques
        Chart.defaults.global.defaultFontColor = "rgba(245,245,245,0.9)";
    </script>

    <?php
    include('../../Modele/stats.php');
    $stats = new Statistiques; // Objet contenant toutes les informations nécessaires
    ?>
</head>

<body>
    <input type="image" class="return" src="../../img/return.png" onclick="history.replaceState(null,null,'../../index.php');location.reload();">
    <h1 class="title">Statistiques</h1>
    <main class="stats">
        <article class="stat"> <!-- Histogramme répartition hommes / femmes -->
            <canvas id="Histo_HF" width="400" height="600"></canvas>
            <script>
                var ctx = document.getElementById('Histo_HF');
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: ['Hommes', 'Femmes'],
                        datasets: [{
                            data: [<?php echo $stats->get_nbHommes();?>, <?php echo $stats->get_nbFemmes();?>],
                            backgroundColor: [
                                'rgba(54, 162, 235, 0.3)',
                                'rgba(255, 99, 132, 0.3)'
                            ],
                            borderColor: [
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 99, 132, 1)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        responsive: true,
                        title: {
                            display: true,
                            text: 'Répartition hommes / femmes'
                        },
                        legend: {
                            display: false
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }
                });
            </script>
        </article>

        <article class="stat"> <!-- Histogramme inscrits par tranche d'âges -->
        <canvas id="Histo_abo_ages" width="400" height="600"></canvas>
            <script>
            var ctx = document.getElementById('Histo_abo_ages');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ['18-24 ans', '24-45 ans', '45-65 ans', 'plus de 65 ans'],
                    datasets: [{
                        data: [<?php echo $stats->get_nb18_24Ans();?>,<?php echo $stats->get_nb24_45Ans();?>,
                               <?php echo $stats->get_nb45_65Ans();?>,<?php echo $stats->get_nb65_plusAns();?>],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.3)',
                            'rgba(54, 162, 235, 0.3)',
                            'rgba(255, 99, 132, 0.3)',
                            'rgba(54, 162, 235, 0.3)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    responsive: true,
                    title: {
                            display: true,
                            text: 'Inscrits par tranche d\'âge'
                    },
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
            </script>
        </article>

        <article class="stat"> <!-- Maison la plus gourmande pour un mois donné en fonction du nombre d'habtitants -->
            <form method="POST" action="">
                <select name="mois" id="select-mois-stats">
                    <option value="">Choisir un mois</option>
                    <option value="1">Janvier</option>
                    <option value="2">Février</option>
                    <option value="3">Mars</option>
                    <option value="4">Avril</option>
                    <option value="5">Mai</option>
                    <option value="6">Juin</option>
                    <option value="7">Juillet</option>
                    <option value="8">Août</option>
                    <option value="9">Septembre</option>
                    <option value="10">Octobre</option>
                    <option value="11">Novembre</option>
                    <option value="12">Décembre</option>
                </select>
            </form>
            <table id="table-gourmand-maison-ressources">
                <tr>
                    <th>Ressource</th>
                    <th>Maison la plus gourmande</th>
                    <th>Quantité</th>
                </tr>

            </table>
            <script type="text/javascript"> // Traitement Ajax du choix du mois
                $(document).ready(function() {
                    $('#select-mois-stats').on('change', function() {
                        if (document.getElementById("select-mois-stats").value){
                            var mois = document.getElementById("select-mois-stats").value;
                            
                            // Envoie le mois choisi sur une page s'occupant du traitement
                            $.ajax({
                                type: 'post',
                                url: '../../Controleur/stats/select-mois-stats.php',
                                data: {
                                    mois:mois,
                                },
                                success: function (response) { // Exécuté une fois que le traitement est terminé
                                    /*
                                    //document.write(response);
                                    $res = JSON.parse(response); // Récupère le résultat envoyé par la page de traitement
                                    // Parcours et affiche la liste des ressources
                                    for(var [key, value] of Object.entries(response)){
                                        document.getElementById("table-gourmand-maison-ressources").innerHTML = 
                                            '<tr><td>'+key+'</td><td>'+Object.keys(value)[0]+'</td><td>'+value[0]+'</td></tr>';
                                    }
                                    */
                                }
                            });
                        }
                    });
                });
            </script>
        </article>
    </main>
</body>

</html>