<!DOCTYPE html>
<html lang="fr">

<head>
    <meta CHARSET="UTF-8">
    <link rel="icon" href="../../img/economy.jpg" type="image/x-icon" />
    <title>Inscription</title>
    <link rel="stylesheet" href="../../Vue/style.css" type="text/css">
    <script src='../../Controleur/script.js'></script>
</head>

<body>
    <div id="dialogoverlay"></div>
    <div id="dialogbox">
        <div>
            <div id="dialogboxhead"></div>
            <div id="dialogboxbody">
            </div>
            <div id="dialogboxfoot"></div>
        </div>
    </div>
    <input type="image" class="return" src="../../img/return.png" onclick="history.replaceState(null,null,'../../Vue/utilisateur/utilisateur_principale_index.php');location.reload();">
    <h1 class="title">Créer une piece</h1>
    <table class="login">
        <form method="post" onsubmit="confirm_all_piece()" >
            <tr>
                <td>
                    <label for="Libelle_piece">Libelle Piece</label>
                </td>
                <td>
                    <input type="text" name="Libelle_piece" id="Libelle_piece"  onkeyup="libelle_piece_check()" >
                </td>
                <td>
                    <span id="Libelle_piece_label"></span>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="typepiece">Type Piece</label>
                </td>
                <td>
                    <select name="typepiece" id="region-select">
                        <?php 
                            include("../../Controleur/piece/ajax_typepiece.php");
                            $liste = ListeTypePieces();
                            foreach ($liste as $value) {
                                echo "
                                <option value=\"".$value["IdTypePiece"]."\">".$value["LibelleTP"]."</option>
                                ";
                            }
                            
                        ?>
                    </select>
                </td>
                <td>
                    <span id="idpiece_label"></span>
                </td>
            </tr>
    </table>
    <input type="submit" value="Valider" name="submit" id="submit">
    </form>
    <?php include "../../Controleur/piece/piece_creation_data.php" ?>
</body>

</html>