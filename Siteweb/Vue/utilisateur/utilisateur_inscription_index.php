<!DOCTYPE html>
<html lang="fr">

<head>
    <meta CHARSET="UTF-8">
    <link rel="icon" href="../../img/economy.jpg" type="image/x-icon" />
    <title>Inscription</title>
    <link rel="stylesheet" href="../../Vue/style.css" type="text/css">
    <script src='../../Controleur/script.js'></script>
</head>

<body>
    <div id="dialogoverlay"></div>
    <div id="dialogbox">
        <div>
            <div id="dialogboxhead"></div>
            <div id="dialogboxbody">
            </div>
            <div id="dialogboxfoot"></div>
        </div>
    </div>
    <input type="image" class="return" src="../../img/return.png" onclick="history.replaceState(null,null,'../../Vue/utilisateur/utilisateur_connexion_index.php');location.reload();">
    <h1 class="title">Créer votre compte</h1>
    <table class="login">
        <form method="post" onsubmit="naissance_check();confirm_all();" >
            <tr>
                <td>
                    <label for="nm">Nom</label>
                </td>
                <td>
                    <input type="text" name="nm" id="nm" onkeyup="nm_check()">
                </td>
                <td>
                    <span id="nm_label"></span>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="prnm">Prenom</label>
                </td>
                <td>
                    <input type="text" name="prnm" id="prnm" onkeyup="prnm_check()">
                </td>
                <td>
                    <span id="prnm_label"></span>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="email">Email</label>
                </td>
                <td>
                    <input type="text" name="email" id="email" onkeyup="email_check()">
                </td>
                <td>
                    <span id="email_label"></span>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="telephone">Téléphone</label>
                </td>
                <td>
                    <input type="text" name="telephone" id="telephone" maxlength="9" placeholder="Sans indicatif ! : 612154896 " size="9px" onkeyup="phone_check()">
                </td>
                <td>
                    <span id="telephone_label"></span>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="user">Nom d'utilisateur</label>
                </td>
                <td>
                    <input type="text" name="user" id="user" placeholder="Exemple:user008" onkeyup="user_check()">
                </td>
                <td>
                    <span id="user_label"></span>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="password" placeholder="Exemple:User_008">Mot de passe</label>
                </td>
                <td>
                    <input type="password" name="password" id="password" placeholder="Exemple:User_008" onkeyup="password_check()">
                </td>
                <td>
                    <span id="password_label"></span>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="confirm">Confirmer le mot de passe</label>
                </td>
                <td>
                    <input type="password" name="confirm" id="confirm" onkeyup="confirm_pass()">
                </td>
                <td>
                    <span id="confirm_label"></span>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="naissance">Date de naissance</label>
                </td>
                <td>
                    <input type="date" name="naissance" id="naissance">
                </td>
                <td>
                    <span id="naissance_label"></span>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="sexe">Sexe</label>
                </td>
                <td>
                    <label for="1">Homme</label><input type="radio" name="sexe" id="1" value="Homme">
                    <label for="2">Femme</label><input type="radio" name="sexe" id="2" value="Femme">
                </td>
                <td>
                    <span id="sexe_label"></span>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="admin">Administrateur ?</label>
                </td>
                <td>
                    <label for="oui">Oui</label><input type="radio" name="admin" id="oui" value=1>
                    <label for="non">Non</label><input type="radio" name="admin" id="non" value=0>
                </td>
                <td>
                    <span id="sexe_label"></span>
                </td>
            </tr>
    </table>
    <input type="submit" value="Valider" name="submit" id="submit">
    </form>
    <?php include "../../Controleur/utilisateur/utilisateur_inscription_data.php" ?>
</body>

</html>