<?php
session_start();
if (!isset($_SESSION['login_success']) || !$_SESSION['login_success']) {
	echo "<script>location.replace('../../Controleur/utilisateur/connexion_deconnexion.php');</script>";
}
?>
<!DOCTYPE html>
<html>

<head>
	<meta CHARSET="UTF-8">
	<link rel="icon" href="../../img/economy.jpg" type="image/x-icon" />
	<title>Accueil</title>
	<link rel="stylesheet" href="../style.css" type="text/css">
	 <script src='../../Controleur/script.js'></script>
</head>

<body>
	<div id="dialogoverlay"></div>
	<div id="dialogbox">
		<div>
			<div id="dialogboxhead"></div>
			<div id="dialogboxbody">
			</div>
			<div id="dialogboxfoot"></div>
		</div>
	</div>
	<input type="image" class="return" src="../../img/return.png" onclick="history.replaceState(null,null,'../../index.php');location.reload();">
	<?php
	echo "<h1 style='	
    color:white;
	font-family:tahoma;
	font-size:25px;
	text-decoration:underline;
	text-align:center;
	text-shadow: 
	3px 3px 0 black,
    -1px -1px 0 black,  
     1px -1px 0 black,
    -1px 1px 0 black,
     1px 1px 0 black;'
	 >Bienvenue cher(e) " . $_SESSION['nom'] . " " . $_SESSION['prenom'] . "<br><br></h1>";

	echo "<a class='button' href='utilisateur_modifier_index.php?id=" . $_SESSION['IdUtilisateur'] . "&nom=" . $_SESSION['nom'] . "&prenom=" . $_SESSION['prenom'] . "&user=" . $_SESSION['user'] . "&datecreation=" . $_SESSION['datecreation'] . "&email=" . $_SESSION['email'] . "&naissance=" . $_SESSION['naissance'] . "&telephone=" . $_SESSION['telephone'] . "&GenreU=" . $_SESSION['GenreU'] . "&EtatU=" . $_SESSION['EtatU'] . "&admin=" . $_SESSION['admin'] . "'>Modifier vos données</a>";

	if ($_SESSION['admin'] == 1) {
		echo "<br><br><a class='button' href='../stats/administrateur_stats_index.php'>Voir les statistiques</a><br><br>";
	}

	if (isset($_REQUEST["modif"])) {
		echo "<h4 style='color:rgb(63,169,95);font-family:tahoma;		text-shadow: 
	1px 1px 0 black,
    -1px -1px 0 black,  
     1px -1px 0 black,
    -1px 1px 0 black,
	1px 1px 0 black;'>Compte modifié avec réussite</h4>";
	}
	
		if (isset($_REQUEST["ajout"])) {
		echo "<h4 style='color:rgb(63,169,95);font-family:tahoma;		text-shadow: 
	1px 1px 0 black,
    -1px -1px 0 black,  
     1px -1px 0 black,
    -1px 1px 0 black,
	1px 1px 0 black;'>Ajout réussi</h4>";
	}
	 
	 	if (isset($_REQUEST["supp"])) {
		echo "<h4 style='color:rgb(63,169,95);font-family:tahoma;		text-shadow: 
	1px 1px 0 black,
    -1px -1px 0 black,  
     1px -1px 0 black,
    -1px 1px 0 black,
     1px 1px 0 black;'>Suppression réussie</h4>";
	}
		 	if (isset($_REQUEST["err"])) {
		echo "<h4 style='color:rgb(255,0,0);font-family:tahoma;		text-shadow: 
	1px 1px 0 black,
    -1px -1px 0 black,  
     1px -1px 0 black,
    -1px 1px 0 black,
     1px 1px 0 black;'>Erreur</h4>";
	}
	
	?>

	<form method="post" action="">

		<table class="menu">
			<tr class="menu">

				<th class="menu">Libelle</th>
				<th class="menu">Type</th>
				<th class="menu">Degre Citoyen</th>
				<th class="menu">Degre Securite</th>
				<th class="menu">Consulter</th>
				<th class="menu">Modifier</th>
				<th class="menu">Supprimer</th>
			</tr>
			<?php
				include("../../Controleur/appartement/ajax_appartement.php");
				$liste = ListeAppartementUtilisateur($_SESSION['IdUtilisateur']);
				foreach ($liste as $value) {
					echo "
					<tr class=\"menu\">
						<td class=\"menu\">".$value["LibelleA"]."</td>
						<td class=\"menu\">".$value["NomTypeAppartement"]."</td>
						<td class=\"menu\">".$value["DegreCitoyenA"]."</td>
						<td class=\"menu\">".$value["DegreSecuriteA"]."</td>
						<td class=\"menu\"><a class=\"cc\" href='../piece/consulter_piece_index.php?IdAppart=".$value['IdAppartement']."' onclick=\"\">Consulter</a></td>
						<td class=\"menu\"><a class=\"cc\" href='../../Controleur/appartement/find_data_appartement.php?IdAppart=".$value['IdAppartement']."' >Modifier</a></td>
						<td class=\"menu\"><a class=\"cc\" href='#' onclick='Alert.delete_appart(".$value['IdAppartement'].");'>Supprimer</a></td>
					</tr>
					";
					
				}
			?>
		</table>
	</form>
	<br><br><a class='button' href="../appartement/selection_adresse_index.php">Créer un Appartement</a>
	<br><br><a class='button' href="../../Controleur/utilisateur/connexion_deconnexion.php">Deconnection</a>
</body>
</html>