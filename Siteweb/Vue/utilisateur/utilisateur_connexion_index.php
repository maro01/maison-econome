<?php

session_start();
if (isset($_SESSION['login_success'])) {
    header("location:utilisateur_principale_index.php");
    exit;
}

?>

<!DOCTYPE html>
<html>

<head>
    <meta CHARSET="UTF-8">
    <link rel="icon" href="../../img/economy.jpg" type="image/x-icon" />
    <title>Connection</title>
    <link rel="stylesheet" href="../style.css" type="text/css">
</head>

<body>
    <input type="image" class="return" src="../../img/return.png" onclick="history.replaceState(null,null,'../../index.php');location.reload();">
    <h1 class="title">Bienvenue</h1>
    <table class="connection">
        <form method="post" action="../../Controleur/utilisateur/connexion_checking.php">
            <tr>
                <td>
                    <label for="user">Nom d'utilisateur</label>
                </td>
                <td>
                    <input type="text" name="user" id="user">
                </td>
            </tr>
            <tr>
                <td>
                    <label for="password">Mot de passe</label>
                </td>
                <td>
                    <input type="password" name="password" id="password">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" value="Valider" name="submit" id="submit">
                </td>
            </tr>
        </form>
    </table>
    <a class="shadowed" href="../../Vue/utilisateur/Utilisateur_inscription_index.php">Vous n'avez pas de compte ?</a>
    <?php
    if (isset($_REQUEST['erreur'])) {
        echo "<h4 class='errorConnexion' >Nom d'utilisateur ou mot de passe sont incorrectes</h4>";
        unset($_REQUEST['erreur']);
    }
    if (isset($_REQUEST['supprimer'])) {
        echo "<h4 class='errorConnexion' >Votre compte est supprimé</h4>";
        unset($_REQUEST['supprimer']);
    }
    if (isset($_REQUEST['supprimer_erreur'])) {
        echo "<h4 class='errorConnexion' >Erreur: Suppression erronée</h4>";
        unset($_REQUEST['supprimer_erreur']);
    }
    ?>
</body>

</html>