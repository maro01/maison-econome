<?php
session_start();
if (!isset($_SESSION['login_success']) || !$_SESSION['login_success']) {
	echo "<script>location.replace('../../Controleur/utilisateur/connexion_deconnexion.php');</script>";
}
?>
<!DOCTYPE html>
<html>

<head>
	<meta CHARSET="UTF-8">
	<link rel="icon" href="../../img/economy.jpg" type="image/x-icon" />
	<title>Modification d'informations</title>
	<link rel="stylesheet" href="../style.css" type="text/css">
	<script src='../../Controleur/script.js'></script>
</head>

<body onload="myfunction1()">
	<div id="dialogoverlay"></div>
	<div id="dialogbox">
		<div>
			<div id="dialogboxhead"></div>
			<div id="dialogboxbody">
			</div>
			<div id="dialogboxfoot"></div>
		</div>
	</div>
	<input type="image" class="return" src="../../img/return.png" onclick="history.replaceState(null,null,'utilisateur_principale_index.php');location.reload();">
	<h1 class="title">Modifier votre compte</h1>
	<?php

	echo "<a class='button' href='#'  onclick='Alert.deletePop(" . $_REQUEST['id'] . ");' >Supprimer votre compte</a>";

	?>
	<table class="login">
		<form method="post" onsubmit="naissance_check();confirm_all();">
			<tr>
				<td>
					<label for="nm">Nom</label>
				</td>
				<td>
					<input type="text" name="nm" id="nm" onkeyup="nm_check()">
				</td>
				<td>
					<span id="nm_label"></span>
				</td>
			</tr>
			<tr>
				<td>
					<label for="prnm">Prenom</label>
				</td>
				<td>
					<input type="text" name="prnm" id="prnm" onkeyup="prnm_check()">
				</td>
				<td>
					<span id="prnm_label"></span>
				</td>
			</tr>
			<tr>
				<td>
					<label for="email">Email</label>
				</td>
				<td>
					<input type="text" name="email" id="email" onkeyup="email_check()">
				</td>
				<td>
					<span id="email_label"></span>
				</td>
			</tr>
			<tr>
				<td>
					<label for="telephone">Téléphone</label>
				</td>
				<td>
					<input type="text" name="telephone" id="telephone" maxlength="9" placeholder="Sans indicatif ! : 612154896 " size="9px" onkeyup="phone_check()">
				</td>
				<td>
					<span id="telephone_label"></span>
				</td>
			</tr>
			<tr>
				<td>
					<label for="user">Nom d'utilisateur</label>
				</td>
				<td>
					<input type="text" name="user" id="user" placeholder="Exemple:user008" onkeyup="user_check()">
				</td>
				<td>
					<span id="user_label"></span>
				</td>
			</tr>
			<tr>
				<td>
					<label for="password" placeholder="Exemple:User_008">Mot de passe</label>
				</td>
				<td>
					<input type="text" name="password" id="password" placeholder="Exemple:User_008" onkeyup="password_check()">
				</td>
				<td>
					<span id="password_label"></span>
				</td>
			</tr>
			<tr>
				<td>
					<label for="new_password" placeholder="Exemple:User_008">Nouveau mot de passe</label>
				</td>
				<td>
					<input type="text" name="new_password" id="new_password" placeholder="Exemple:User_008" onkeyup="new_password_check()">
				</td>
				<td>
					<span id="new_password_label"></span>
				</td>
			</tr>
			<tr>
				<td>
					<label for="confirm_new_password">Confirmer le nouveau mot de passe</label>
				</td>
				<td>
					<input type="text" name="confirm_new_password" id="confirm_new_password" onkeyup="confirm_pass()">
				</td>
				<td>
					<span id="confirm_new_password_label"></span>
				</td>
			</tr>
			<tr>
				<td>
					<label for="naissance">Date de naissance</label>
				</td>
				<td>
					<input type="date" name="naissance" id="naissance">
				</td>
				<td>
					<span id="naissance_label"></span>
				</td>
			</tr>
			<tr>
				<td>
					<label for="sexe">Sexe</label>
				</td>
				<td>
					<label for="1">Homme</label><input type="radio" name="sexe" id="1" value="Homme">
					<label for="2">Femme</label><input type="radio" name="sexe" id="2" value="Femme">
				</td>
				<td>
					<span id="sexe_label"></span>
				</td>
			</tr>
			<tr>
				<td>
					<label for="admin">Administrateur ?</label>
				</td>
				<td>
					<label for="oui">Oui</label><input type="radio" name="admin" id="oui" value=1>
					<label for="non">Non</label><input type="radio" name="admin" id="non" value=0>
				</td>
				<td>
					<span id="sexe_label"></span>
				</td>
			</tr>
	</table>
	<input type="submit" value="Valider" name="submit" id="submit">
	</form>
	<?php include '../../Controleur/utilisateur/modifier_data.php' ?>
</body>

</html>