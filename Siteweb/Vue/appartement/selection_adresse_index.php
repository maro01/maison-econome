<?php
session_start();
if (!isset($_SESSION['login_success']) || !$_SESSION['login_success']) {
    echo "<script>location.replace('../../Controleur/utilisateur/connexion_deconnexion.php');</script>";
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta CHARSET="UTF-8">
    <link rel="icon" href="../../img/economy.jpg" type="image/x-icon"/>
    <title>Saisie d'addresse</title>
    <link rel="stylesheet" href="../../Vue/style.css" type="text/css">
    <script src='../../Controleur/script.js'></script>
</head>

<body onload="adresse_change();">
<div id="dialogoverlay"></div>
<div id="dialogbox">
    <div>
        <div id="dialogboxhead"></div>
        <div id="dialogboxbody">
        </div>
        <div id="dialogboxfoot"></div>
    </div>
</div>
<input type="image" class="return" src="../../img/return.png"
       onclick="history.replaceState(null,null,'../../Vue/utilisateur/utilisateur_principale_index.php');location.reload();">
<h1 class="title">Selectionnez l'addresse de l'appartement à créer</h1>
<table class="login">
    <form method="post" onsubmit="confirm_all_adresse()" action="appartement_creation_index.php">
        <td>
            <label for="region">Region</label>
        </td>
        <td>

            <select name="region" id="region" class="select" onchange="adresse_change();">
                <optgroup label="Choisissez une region">
                    <?php
                    $con = mysqli_connect("127.0.0.1", "root", "", "maison_econome");
                    if ($con) {
                        $result = mysqli_query($con, 'select * from region');
                        if ($result) {
                            while ($row = mysqli_fetch_assoc($result)) {
                                echo "<option value='" . $row["IdRegion"] . "'>" . $row["NomRegion"] . "</option>";
                            }
                        }
                    }
                    mysqli_close($con);
                    ?>
                </optgroup>
            </select><br>
        </td>
        </tr>
        <tr>
            <td>
                <label for="depart">Departement</label>
            </td>
            <td>
                <select name="depart" id="depart" class="select" onchange="depart_ville();depart_codepostal();">
                    <optgroup label="Choisissez un departement">
                    </optgroup>
                </select><br>
            </td>
        </tr>
        <tr>
            <td>
                <label for="depart">Ville</label>
            </td>
            <td>
                <select name="ville" id="ville" class="select" onchange="ville_rue();depart_codepostal();">
                    <optgroup label="Choisissez une ville">
                    </optgroup>
                </select><br>
            </td>
        </tr>
        <tr>
            <td>
                <label for="codepostal">Code postal</label>
            </td>
            <td>
                <select name="codepostal" id="codepostal" class="select">
                    <optgroup label="Choisissez un code postal">
                    </optgroup>
                </select><br>
            </td>
        </tr>

        <tr>
            <td>
                <label for="Rue">Rue</label>
            </td>
            <td>
                <select name="rue" id="rue" class="select"  >
                    <optgroup label="Choisissez une rue">
                    </optgroup>
                </select><br>
            </td>
        </tr>
        <input type="submit" value="Valider" name="submit" id="submit">
    </form>
</table>
    <p>
        <h1 class="title"> Vous ne trouvez pas votre adresse ? </h1>
        <a class='button' style="display: block ;margin: auto; text-align: center" href="../adresse_departement_ville/addresse_creation_index.php">Créer une adresse</a>
    </p>
</body>
</html>