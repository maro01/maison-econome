<?php
session_start();
if (!isset($_SESSION['login_success']) || !$_SESSION['login_success']) {
    echo "<script>location.replace('../../Controleur/utilisateur/connexion_deconnexion.php');</script>";
}
if (isset($_GET['IdAppartement'])){
    $_SESSION["IdAppartement"] = $_GET['IdAppartement'];
}

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta CHARSET="UTF-8">
    <link rel="icon" href="../../img/economy.jpg" type="image/x-icon"/>
    <title>Modification d'appartement</title>
    <link rel="stylesheet" href="../../Vue/style.css" type="text/css">
    <script src='../../Controleur/script.js'></script>
</head>

<body onload="myfunction2()">
<div id="dialogoverlay"></div>
<div id="dialogbox">
    <div>
        <div id="dialogboxhead"></div>
        <div id="dialogboxbody">
        </div>
        <div id="dialogboxfoot"></div>
    </div>
</div>
<input type="image" class="return" src="../../img/return.png"
       onclick="history.replaceState(null,null,'../../Vue/utilisateur/utilisateur_principale_index.php');location.reload();">
<h1 class="title">Modification d'appartement  </h1>
<table class="login">
    <form method="post" onsubmit="confirm_all_appart()" action="../../Controleur/appartement/appartement_modification_data.php">
    <tr>
                <td>
                    <label for="typeappartement">Type Appartement</label>
                </td>
                <td>
                    <select name="typeappartement" id="typeappartement">
                        <?php 
                            include("../../Controleur/appartement/ajax_appartement.php");
                            
                            $liste = ListeTypeAppartements();
                            foreach ($liste as $value) {
                                echo "
                                <option value=\"".$value["IdTypeAppartement"]."\">".$value["NomTypeAppartement"]."</option>
                                ";
                            }
                            
                        ?>
                    </select>
                </td>
                <td>
                    <span id="idappartement_label"></span>
                </td>
            </tr>
        <tr>
            <td>
                <label for="degcit">Degre Citoyen de l'appartement</label>
            </td>
            <td>
                <input type="text" name="degcit" id="degcit" onkeyup="degcit_appart_check()">
            </td>
            <td>
                <span id="degcit_label"></span>
            </td>
        </tr>
        <tr>
            <td>
                <label for="degsec">Degre de Securite de l'appartement</label>
            </td>
            <td>
                <input type="text" name="degsec" id="degsec" onkeyup="degsec_appart_check()">
            </td>
            <td>
                <span id="degsec_label"></span>
            </td>
        </tr>
        <tr>
            <td>
                <label for="libelle">Libelle de l'appartement</label>
            </td>
            <td>
                <input type="text" name="libelle" id="libelle" onkeyup="libelle_appart_check()">
            </td>
            <td>
                <span id="libelle_label"></span>
            </td>
        </tr>

        <input type="submit" value="Valider" name="submit" id="submit">
    </form>
</table>
</body>

</html>