<?php
// Récupère et met à disposition les données nécessaires à la page administrateur_stats_index.php
class Statistiques
{

    private $nbHommes;
    private $nbFemmes;

    private $nb18_24Ans;
    private $nb24_45Ans;
    private $nb45_65Ans;
    private $nb65_plusAns;

    private $liste_ressources;
    private $gourmand_ressource;

    public function __construct()
    {
        include("connexion_bd.php"); // Connexion à la base de données

        // Graphe rapport hommes femmes
        $result = mysqli_query($bdd, "SELECT COUNT(*) AS nbHommes
                                      FROM Utilisateur
                                      WHERE GenreU='Homme'");
        $this->nbHommes = mysqli_fetch_assoc($result)['nbHommes'];
        $result = mysqli_query($bdd, "SELECT COUNT(*) AS nbFemmes
                                      FROM Utilisateur
                                      WHERE GenreU='Femme'");
        $this->nbFemmes = mysqli_fetch_assoc($result)['nbFemmes'];

        // Graphe tranche d'âges
        $result = mysqli_query($bdd, "SELECT DateNaissanceU
                                      FROM Utilisateur");
        while ($row = mysqli_fetch_array($result)){
            // Convertit la date MySQL en date PHP
            $phpdate = strtotime($row['DateNaissanceU']);
            $strDateNaiss = date('Y-m-d', $phpdate);
            
            $d1 = new DateTime($strDateNaiss);
            $d2 = new DateTime();
            $diff = $d2->diff($d1);
            
            $age =  $diff->y;
            if($age >= 18 && $age < 24){
                $this->nb18_24Ans++;
            }
            elseif($age >= 24 && $age < 45){
                $this->nb24_45Ans++;
            }
            elseif($age >= 45 && $age < 65){
                $this->nb45_65Ans++;
            }
            elseif($age >= 65){
                $this->nb65_plusAns++;
            }
        }

        // Stats de la maison la plus gourmande pour un mois pour chaque ressource
        $this->liste_ressources = array();
        $result = mysqli_query($bdd, 'SELECT NomRessource
                                      FROM ressource');
        while ($row = mysqli_fetch_array($result)){
            array_push($this->liste_ressources, $row);
        }
        $this->gourmand_ressource = [];

        mysqli_close($bdd);
    }

    public function get_NomRessource_from_id($bdd, $id){
        $result = mysqli_query($bdd, 'SELECT NomRessource
                                         FROM ressource
                                         WHERE IdRessource = '.$id);
        return mysqli_fetch_array($result)['NomRessource'];
    }

    public function get_nbHommes()
    {
        return $this->nbHommes;
    }

    public function get_nbFemmes()
    {
        return $this->nbFemmes;
    }

    public function get_nb18_24Ans()
    {
        return $this->nb18_24Ans;
    }

    public function get_nb24_45Ans()
    {
        return $this->nb24_45Ans;
    }

    public function get_nb45_65Ans()
    {
        return $this->nb45_65Ans;
    }

    public function get_nb65_plusAns()
    {
        return $this->nb65_plusAns;
    }

    public function get_liste_ressources(){
        return $this->liste_ressources;
    }

    public function get_gourmand_ressource(){
        return $this->gourmand_ressource;
    }

    public function set_gourmand_ressource($liste){
        $this->gourmand_ressource = $liste;
    }
}
?>