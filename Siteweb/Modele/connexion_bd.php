<?php
// Connexion à la base de données
// Activation de l'affichage des erreurs php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
header('content-type: text/html; charset=utf-8');

try
{
  $bdd = mysqli_connect("127.0.0.1", "root", "", "maison_econome");
  mysqli_set_charset($bdd, "utf8");
}
catch(Exception $e)
{
  die('Erreur : '.$e->getMessage());
}

// Définit le fuseau horaire
date_default_timezone_set('Europe/Paris');
?>
