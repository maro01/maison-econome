<?php
    include("../../Modele/connexion_bd.php");

    // Traitement des statistiques sur la maison la plus gourmande en ressource pour un mois donné
    if(isset($_POST['mois'])){
        // Crée l'interval du mois
        $mois = $_POST['mois'];
        $annee = date("Y");
        $dateDeb = date("Y-m-d", mktime(0, 0, 0, $mois,   1, $annee));
        $dateFin = date("Y-m-d", mktime(0, 0, 0, $mois+1, 0, $annee));

        $liste_ressources = get_liste_ressources($bdd);

        $liste_maison_gourmande_par_ressource = []; // NomRessource => [IdAppart => conso]
        $ressource = array();
        while ($ressource = mysqli_fetch_array($liste_ressources)) {
            $liste_maison_gourmande_par_ressource[$ressource['NomRessource']] = 
                get_appart_plus_gourmand_ressource_dans_interval($bdd, (int)$ressource['IdRessource'], $dateDeb, $dateFin);
        }
        echo var_dump(json_encode($liste_maison_gourmande_par_ressource)); // Envoie le résultat à la page initiale
    }

    function get_liste_ressources($bdd){
        return mysqli_query($bdd, 'SELECT IdRessource, NomRessource
                                   FROM ressource;');
    }

    function get_appareil_conso_ressource_dans_interval($bdd, $id_appareil, $id_ressource, $dateDeb, $dateFin){
        $query_res = mysqli_query($bdd, 'SELECT SUM(Qte) AS conso_appareil
                                         FROM consommer
                                         WHERE DateDebR <= \''.$dateFin.'\' AND DateFinR >= \''.$dateDeb.'\' AND
                                         IdAppareil = '.$id_appareil.' AND IdRessource = '.$id_ressource.';');
        $row = mysqli_fetch_array($query_res);
        return $row['conso_appareil'];
    }

    function get_piece_conso_ressource_dans_interval($bdd, $id_piece, $id_ressource, $dateDeb, $dateFin){
        $query_res = mysqli_query($bdd, 'SELECT IdAppareil
                                         FROM appareil
                                         WHERE IdPiece = '.$id_piece.';');
        
        $total_conso = 0;
        while ($row = mysqli_fetch_array($query_res)) {
            $total_conso += get_appareil_conso_ressource_dans_interval($bdd, (int)$row['IdAppareil'], $id_ressource, $dateDeb, $dateFin);
        }
        return $total_conso;
    }

    function get_appart_conso_ressource_dans_interval($bdd, $id_appart, $id_ressource, $dateDeb, $dateFin){
        $query_res = mysqli_query($bdd, 'SELECT IdPiece
                                         FROM piece
                                         WHERE IdAppartement = '.$id_appart.';');

        $total_conso = 0;
        while ($row = mysqli_fetch_array($query_res)) {
            $total_conso += get_piece_conso_ressource_dans_interval($bdd, (int)$row['IdPiece'], $id_ressource, $dateDeb, $dateFin);
        }
        return $total_conso;
    }

    function get_appart_plus_gourmand_ressource_dans_interval($bdd, $id_ressource, $dateDeb, $dateFin){
        $query_res = mysqli_query($bdd, 'SELECT IdAppartement
                                         FROM appartement;');
        $max_id_appart = NULL;
        $max_appart_conso = 0;
        while ($row = mysqli_fetch_array($query_res)) {
            $appart_conso = get_appart_conso_ressource_dans_interval($bdd, (int)$row['IdAppartement'], $id_ressource, $dateDeb, $dateFin);
            if ($appart_conso >= $max_appart_conso){
                $max_id_appart = $row['IdAppartement'];
                $max_appart_conso = $appart_conso;
            } 
        }
        if($max_appart_conso == 0){
            //$max_id_appart = "Aucun";
        }
        return array($max_id_appart => $max_appart_conso);
    }
?>