<?php
session_start();
if (!isset($_SESSION['login_success']) || !$_SESSION['login_success']) {
    echo "<script>location.replace('../../Controleur/utilisateur/connexion_deconnexion.php');</script>";
}

include("ajax_appartement.php");

$liste = getBatiment($_SESSION["idAdresse"]);

foreach ($liste as $value) {
    $idBat = $value["IdBatiment"];
}
if (isset($_POST['submit'])) {
	
if (!empty($_POST['typeappartement']) && !empty($_POST['degcit']) && !empty($_POST['degsec']) && !empty($_POST['libelle']) && !empty($idBat)){
	
		include("../../Modele/connexion_bd.php");
		include("../../Modele/appartement.php");
		$degcit = $_POST['degcit'];
		$degsec = $_POST['degsec'];
		$libelle = $_POST['libelle'];
		$IdTypeAppartement = $_POST['typeappartement'];


			$A = new Appartement($bdd,$degcit,$degsec,$libelle,$IdTypeAppartement,$idBat,0);
			if ($A) {
				echo "<h4 style='color:rgb(63,169,95);font-family:tahoma;		text-shadow: 
	1px 1px 0 black,
    -1px -1px 0 black,  
     1px -1px 0 black,
    -1px 1px 0 black,
     1px 1px 0 black;'>Insertion réussie</h4>";
			}
			else {	
				echo "<h4 style='color:red;font-family:tahoma;		text-shadow: 
	1px 1px 0 black,
    -1px -1px 0 black,  
     1px -1px 0 black,
    -1px 1px 0 black,
     1px 1px 0 black;'>Erreur : Insertion erronée</h4>";
			}
	}
    header("location:../../Vue/utilisateur/utilisateur_principale_index.php?ajout=true");
}
?>