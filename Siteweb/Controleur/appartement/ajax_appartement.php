<?php
/**
 * Appartement a afficher pour l'utilisateur passé en parametre
 * @param int $idUtilisateur
 * @return array
 */
function ListeAppartementUtilisateur(int $idUtilisateur): array {
    include("../../Modele/connexion_bd.php");
    $res = array();

    $req = mysqli_query($bdd, "select * from Appartement NATURAL JOIN TypeAppartement ");

    if ($req) {
        while ($row = mysqli_fetch_array($req)) {
            array_push($res, $row);
        }
    }

    mysqli_close($bdd);
    return $res;
}

function ListeTypeAppartements(): array {
    include("../../Modele/connexion_bd.php");
    $res = array();

    $req = mysqli_query($bdd, "select * from TypeAppartement");

    if ($req) {
        while ($row = mysqli_fetch_array($req)) {
            array_push($res, $row);
        }
    }

    mysqli_close($bdd);
    return $res;
}

function getBatiment(int $idAdresse): array {
    include("../../Modele/connexion_bd.php");
    $res = array();

    $req = mysqli_query($bdd, "select * from batiment where idAdresse =".$idAdresse);

    if ($req) {
        while ($row = mysqli_fetch_array($req)) {
            array_push($res, $row);
        }
    }

    mysqli_close($bdd);
    return $res;
}


function ListeRegions(): array {
    include("../../Modele/connexion_bd.php");
    $res = array();

    $req = mysqli_query($bdd, "select * from Region");

    if ($req) {
        while ($row = mysqli_fetch_array($req)) {
            array_push($res, $row);
        }
    }

    mysqli_close($bdd);
    return $res;
}

function ListePieceUtilisateur(int $idUtilisateur): array {
    include("../../Modele/connexion_bd.php");
    $res = array();

    $req = mysqli_query($bdd,"select * from piece p ,typePiece t ,appartement a where p.IdTypePiece=t.IdTypePiece and p.IdAppartement=a.IdAppartement");

    if ($req) {
        while ($row = mysqli_fetch_array($req)) {
            array_push($res, $row);
        }
    }

    mysqli_close($bdd);
    return $res;
}

?>