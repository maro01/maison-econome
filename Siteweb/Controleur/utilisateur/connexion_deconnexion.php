<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if (isset($_SESSION['id']))
    unset($_SESSION['id']);

if (isset($_SESSION['nom']))
    unset($_SESSION['nom']);

if (isset($_SESSION['prenom']))
    unset($_SESSION['prenom']);

if (isset($_SESSION['user']))
    unset($_SESSION['user']);

if (isset($_SESSION['password']))
    unset($_SESSION['password']);

if (isset($_SESSION['datecreation']))
    unset($_SESSION['datecreation']);

if (isset($_SESSION['email']))
    unset($_SESSION['email']);

if (isset($_SESSION['naissance']))
    unset($_SESSION['naissance']);

if (isset($_SESSION['telephone']))
    unset($_SESSION['telephone']);

if (isset($_SESSION['GenreU']))
    unset($_SESSION['GenreU']);

if (isset($_SESSION['EtatU']))
    unset($_SESSION['EtatU']);

if (isset($_SESSION['admin']))
    unset($_SESSION['admin']);

if (isset($_SESSION['login_success']))
    unset($_SESSION['login_success']);

if (isset($_REQUEST['erreur'])) {
    echo "<script>location.replace('../../Vue/utilisateur/utilisateur_connexion_index.php?erreur=true')</script>";
} else if (isset($_REQUEST['supprimer'])) {
    echo "<script>location.replace('../../Vue/utilisateur/utilisateur_connexion_index.php?supprimer=true')</script>";
} else if (isset($_REQUEST['supprimer_erreur'])) {
    echo "<script>location.replace('../../Vue/utilisateur/utilisateur_connexion_index.php?supprimer_erreur=true')</script>";
} else
    echo "<script>location.replace('../../Vue/utilisateur/utilisateur_connexion_index.php')</script>";
