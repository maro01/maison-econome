<?php
if (isset($_POST['submit'])) {

	if (
		!empty($_POST['nm']) && !empty($_POST['prnm'])
		&& !empty($_POST['email']) && !empty($_POST['telephone']) && !empty($_POST['user']) && !empty($_POST['password']) && !empty($_POST['naissance'])
		&& !empty($_POST['sexe'])  && ($_POST['admin'] == 1 || $_POST['admin'] == 0)
	) {
		include("../../Modele/connexion_bd.php");
		include("../../Modele/Utilisateur.php");
		$nom = $_POST['nm'];
		$prenom = $_POST['prnm'];
		$email = $_POST['email'];
		$telephone = $_POST['telephone'];
		$user = $_POST['user'];
		$password = $_POST['password'];
		$naissance = $_POST["naissance"];
		$sexe = $_POST["sexe"];
		$admin = $_POST["admin"];
		$user_valide = false;
		$phone_valide = false;
		$email_valide = false;
		$result_email = mysqli_query($bdd, "select Mail from utilisateur where Mail='" . $email . "'");
		if (!$result_email || mysqli_num_rows($result_email) == 0) {
			$email_valide = true;
		}

		$result_user = mysqli_query($bdd, "select Login from utilisateur where Login='" . $user . "'");
		if (!$result_user || mysqli_num_rows($result_user) == 0) {
			$user_valide = true;
		}

		$result_phone = mysqli_query($bdd, "select TelU from utilisateur where TelU='" . $telephone . "'");
		if (!$result_phone || mysqli_num_rows($result_phone) == 0) {
			$phone_valide = true;
		}

		if ($email_valide && $user_valide && $phone_valide) {
			$now = date_create()->format('Y-m-d H:i:s');
			$u = new Utilisateur($bdd,$nom,$prenom,$email,$user,$password,$naissance,$telephone,$sexe,$admin,$now);
			
			echo "<script>location.replace('../../Controleur/utilisateur/connexion_checking.php?submit=yes&user=".$user."&password=".$password."')</script>";

			if ($u) {
				echo "<h4 style='color:rgb(63,169,95);font-family:tahoma;		text-shadow: 
	1px 1px 0 black,
    -1px -1px 0 black,  
     1px -1px 0 black,
    -1px 1px 0 black,
     1px 1px 0 black;'>Compte crée avec réussite</h4>";
			} else {
				echo "<h4 style='color:red;font-family:tahoma;		text-shadow: 
	1px 1px 0 black,
    -1px -1px 0 black,  
     1px -1px 0 black,
    -1px 1px 0 black,
     1px 1px 0 black;'>Erreur : Inscription erronée</h4>";
			}
		}

		if (!$email_valide) {
			echo "<h4 style='color:red;font-family:tahoma;		text-shadow: 
	1px 1px 0 black,
    -1px -1px 0 black,  
     1px -1px 0 black,
    -1px 1px 0 black,
     1px 1px 0 black;'>Erreur : Adresse email déja utilisée</h4>";
		}
		if (!$user_valide) {
			echo "<h4 style='color:red;font-family:tahoma;		text-shadow: 
1px 1px 0 black,
-1px -1px 0 black,  
1px -1px 0 black,
-1px 1px 0 black,
1px 1px 0 black;'>Erreur : Nom d'utilisateur déja utilisée</h4>";
		}
		if (!$phone_valide) {
			echo "<h4 style='color:red;font-family:tahoma;		text-shadow: 
1px 1px 0 black,
-1px -1px 0 black,  
1px -1px 0 black,
-1px 1px 0 black,
1px 1px 0 black;'>Erreur : Adresse téléphonique déja utilisée</h4>";
		}
	}
}

?>