
function region_depart()
{   
	var xmlhttp=new XMLHttpRequest();
	xmlhttp.open("GET","../../Controleur/adresse_ville_departement_region/ajax_region_depart.php?id_region="+document.getElementById('region').value,false);
	xmlhttp.send(null);
	document.getElementById('depart').innerHTML=xmlhttp.responseText;
}

function depart_ville()
{
	var xmlhttp=new XMLHttpRequest();
	xmlhttp.open("GET","../../Controleur/adresse_ville_departement_region/ajax_depart_ville.php?id_depart="+document.getElementById('depart').value,false);	xmlhttp.send(null);
	document.getElementById('ville').innerHTML=xmlhttp.responseText;
}

function depart_codepostal()
{
	var xmlhttp=new XMLHttpRequest();
	xmlhttp.open("GET","../../Controleur/adresse_ville_departement_region/ajax_depart_codepostal.php?id_ville="+document.getElementById('ville').value,false);	xmlhttp.send(null);
	document.getElementById('codepostal').innerHTML=xmlhttp.responseText;
}

function ville_rue()
{
	var xmlhttp=new XMLHttpRequest();
	xmlhttp.open("GET","../../Controleur/adresse_ville_departement_region/ajax_ville_rue.php?id_ville="+document.getElementById('ville').value,false);	xmlhttp.send(null);
	document.getElementById('rue').innerHTML=xmlhttp.responseText;
}


function adresse_change()
{
	region_depart();depart_ville();depart_codepostal();ville_rue();
}

function email_check() {
	var regexp = /^\S*$/;
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(document.getElementById('email').value) && regexp.test(document.getElementById('email').value)) {
		document.getElementById('email_label').style.color = "rgb(42,245,76)";
		document.getElementById('email_label').innerHTML = "Valide";
		return true;
	}
	if (document.getElementById('email').value == '') {
		document.getElementById('email_label').innerHTML = "";
	}
	else {
		document.getElementById('email_label').style.color = "red";
		document.getElementById('email_label').innerHTML = "Invalide";
		return false;
	}
}

function user_check() {
	var regexp = /^\S*$/;
	if (/^(?=.*[a-zA-Z0-9]).{6,20}$/.test(document.getElementById("user").value) && regexp.test(document.getElementById("user").value)) {
		document.getElementById('user_label').style.color = "rgb(42,245,76)";
		document.getElementById('user_label').innerHTML = "Valide";
		return true;
	}
	if (document.getElementById('user').value == '') {
		document.getElementById('user_label').innerHTML = "";
	}
	else {
		document.getElementById('user_label').style.color = "red";
		document.getElementById('user_label').innerHTML = "Invalide";
		return false;
	}
}
// /^[a-zA-Z0-9 '.-].{2,25}$/i
function nm_check() {
	var regexp = /^\S*$/;
	if (/^[a-zA-Z'].{1,15}$/i.test(document.getElementById("nm").value) && regexp.test(document.getElementById("nm").value)) {
		document.getElementById('nm_label').style.color = "rgb(42,245,76)";
		document.getElementById('nm_label').innerHTML = "Valide";
		return true;
	}
	if (document.getElementById('nm').value == '') {
		document.getElementById('nm_label').innerHTML = "";
	}
	else {
		document.getElementById('nm_label').style.color = "red";
		document.getElementById('nm_label').innerHTML = "Invalide";
		return false;
	}
}
   
function phone_check() {
	var regexp = /^\S*$/;
	if (/^[0-9].{8,}$/i.test(document.getElementById("telephone").value) && regexp.test(document.getElementById("telephone").value)) {
		document.getElementById('telephone_label').style.color = "rgb(42,245,76)";
		document.getElementById('telephone_label').innerHTML = "Valide";
		return true;
	}
	if (document.getElementById('telephone').value == '') {
		document.getElementById('telephone_label').innerHTML = "";
		return false;
	}
	else {
		document.getElementById('telephone_label').style.color = "red";
		document.getElementById('telephone_label').innerHTML = "Invalide";
		return false;
	}
}


function prnm_check() {
	var regexp = /^\S*$/;
	if (/^[a-zA-Z'].{1,15}$/i.test(document.getElementById("prnm").value) && regexp.test(document.getElementById("prnm").value)) {
		document.getElementById('prnm_label').style.color = "rgb(42,245,76)";
		document.getElementById('prnm_label').innerHTML = "Valide";
		return true;
	}
	if (document.getElementById('prnm').value == '') {
		document.getElementById('prnm_label').innerHTML = "";
	}
	else {
		document.getElementById('prnm_label').style.color = "red";
		document.getElementById('prnm_label').innerHTML = "Invalide";
		return false;
	}
}
function password_check() {
	var regexp = /^\S*$/;
	var passw = /^(?=.*[!@_*?])(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
	var inputtxt = document.getElementById("password");
	if (inputtxt.value.match(passw) && regexp.test(inputtxt.value)) {
		let val = inputtxt.value;
		if (val.length >= 6 && val.length <= 10) {
			document.getElementById('password_label').style.color = "rgb(255, 153, 0)";
			document.getElementById('password_label').innerHTML = "Faible";
		}
		if (val.length > 10 && val.length <= 15) {
			document.getElementById('password_label').style.color = "rgb(42,245,76)";
			document.getElementById('password_label').innerHTML = "Assez bien";
		}
		if (val.length > 15) {
			document.getElementById('password_label').style.color = "rgb(0, 255, 0)";
			document.getElementById('password_label').innerHTML = "Parfait";
		}
		return true;
	}
	if (document.getElementById('password').value == '') {
		document.getElementById('password_label').innerHTML = "";
	}
	else {
		document.getElementById('password_label').style.color = "red";
		document.getElementById('password_label').innerHTML = "Invalide";
		return false;
	}
}

function new_password_check() {
	var regexp = /^\S*$/;
	var passw = /^(?=.*[!@_*?])(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
	var inputtxt = document.getElementById("new_password");
	if (inputtxt.value.match(passw) && regexp.test(inputtxt.value)) {
		let val = inputtxt.value;
		if (val.length >= 6 && val.length <= 10) {
			document.getElementById('new_password_label').style.color = "rgb(255, 153, 0)";
			document.getElementById('new_password_label').innerHTML = "Faible";
		}
		if (val.length > 10 && val.length <= 15) {
			document.getElementById('new_password_label').style.color = "rgb(42,245,76)";
			document.getElementById('new_password_label').innerHTML = "Assez bien";
		}
		if (val.length > 15) {
			document.getElementById('new_password_label').style.color = "rgb(0, 255, 0)";
			document.getElementById('new_password_label').innerHTML = "Parfait";
		}
		return true;
	}
	if (document.getElementById('new_password').value == '') {
		document.getElementById('new_password_label').innerHTML = "";
	}
	else {
		document.getElementById('new_password_label').style.color = "red";
		document.getElementById('new_password_label').innerHTML = "Invalide";
		return false;
	}
}

function confirm_pass() {
	if (document.getElementById('confirm_new_password').value == document.getElementById('new_password').value) return true;
	else return false;
}
function naissance_check(){	
        let minAge = 18;
            var date = new Date(document.getElementById("naissance").value);
            var today = new Date();

            var timeDiff = Math.abs(today.getTime() - date.getTime());
            var age1 = Math.ceil(timeDiff / (1000 * 3600 * 24)) / 365;
            if (age1>18) return true;
			else 
			{
			Alert.alertage();
		    event.preventDefault();
		    return false;
			}
}

function confirm_all() {
	if (!(email_check() && phone_check() && user_check() && password_check() && new_password_check() && confirm_pass() && nm_check() && prnm_check())) {
		Alert.render();
		event.preventDefault();
		return false;
	}
	else return true;

}

function type_appart_check() {
	if (!isBlank(document.getElementById('type').value)) {
		document.getElementById('type_label').style.color = "rgb(42,245,76)";
		document.getElementById('type_label').innerHTML = "Valide";
		return true;
	}
	else {
		document.getElementById('type_label').style.color = "red";
		document.getElementById('type_label').innerHTML = "Invalide";
		return false;
	}
}

function isEmpty(str) {
    return (!str || 0 === str.length);
}

function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

function degcit_appart_check() {
	
	if (!isBlank(document.getElementById('degcit').value)) {
		document.getElementById('degcit_label').style.color = "rgb(42,245,76)";
		document.getElementById('degcit_label').innerHTML = "Valide";
		return true;
	}
	else {
		document.getElementById('degcit_label').style.color = "red";
		document.getElementById('degcit_label').innerHTML = "Invalide";
		return false;
	}
	
}

function degsec_appart_check() {
	if (!isBlank(document.getElementById('degsec').value)) {
		document.getElementById('degsec_label').style.color = "rgb(42,245,76)";
		document.getElementById('degsec_label').innerHTML = "Valide";
		return true;
	}
	else {
		document.getElementById('degsec_label').style.color = "red";
		document.getElementById('degsec_label').innerHTML = "Invalide";
		return false;
	}
}

function nomrue_check() {
	var regexp = /^\S*$/;
    if (document.getElementById("nomrue").value !='') {
		document.getElementById('nomrue_label').style.color = "rgb(42,245,76)";
		document.getElementById('nomrue_label').innerHTML = "Valide";
		return true;
	}
	if (document.getElementById('nomrue').value == '') {
		document.getElementById('nomrue_label').innerHTML = "";
	}
	else {
		document.getElementById('nomrue_label').style.color = "red";
		document.getElementById('nomrue_label').innerHTML = "Invalide";
		return false;
	}
}

function numrue_check() {
	var regexp = /^\S*$/;
    if (!isNaN(document.getElementById("numrue").value) && document.getElementById('numrue').value != '') {
		document.getElementById('numrue_label').style.color = "rgb(42,245,76)";
		document.getElementById('numrue_label').innerHTML = "Valide";
		return true;
	}
	if (document.getElementById('numrue').value == '') {
		document.getElementById('numrue_label').innerHTML = "";
	}
	else {
		document.getElementById('numrue_label').style.color = "red";
		document.getElementById('numrue_label').innerHTML = "Invalide";
		return false;
	}
}

function confirm_all_adresse() {
	if (!( nomrue_check() && numrue_check() )) {
		Alert.render();
		event.preventDefault();
		return false;
	}
	else return true;

}


function libelle_appart_check() {
	var regexp = /^\S*$/;
	if (document.getElementById("libelle").value !='') {
		document.getElementById('libelle_label').style.color = "rgb(42,245,76)";
		document.getElementById('libelle_label').innerHTML = "Valide";
		return true;
	}
	if (document.getElementById('libelle').value == '') {
		document.getElementById('libelle_label').innerHTML = "";
	}
	else {
		document.getElementById('libelle_label').style.color = "red";
		document.getElementById('libelle_label').innerHTML = "Invalide";
		return false;
	}
}



function idbat_appart_check() {
	if (!isNaN(document.getElementById("idbat").value) && document.getElementById("idbat").value != '') {
		document.getElementById('idbat_label').style.color = "rgb(42,245,76)";
		document.getElementById('idbat_label').innerHTML = "Valide";
		return true;
	}
	if (document.getElementById('idbat').value == '') {
		document.getElementById('idbat_label').innerHTML = "";
		return false;
	}
	else {
		document.getElementById('idbat_label').style.color = "red";
		document.getElementById('idbat_label').innerHTML = "Invalide";
		return false;
	}
}
function confirm_all_appart() {
	if (!( degcit_appart_check() && degsec_appart_check() && libelle_appart_check() )) {
		Alert.render();
		event.preventDefault();
		return false;
	}
	else return true;

}

function libelle_piece_check() {
	var regexp = /^\S*$/;
	if (document.getElementById("Libelle_piece").value !='') {
		document.getElementById('Libelle_piece_label').style.color = "rgb(42,245,76)";
		document.getElementById('Libelle_piece_label').innerHTML = "Valide";
		return true;
	}
	if (document.getElementById('Libelle_piece').value == '') {
		document.getElementById('Libelle_piece_label').innerHTML = "";
	}
	else {
		document.getElementById('Libelle_piece_label').style.color = "red";
		document.getElementById('Libelle_piece_label').innerHTML = "Invalide";
		return false;
	}
}

function confirm_all_piece() {
	if (!(libelle_piece_check() )) {
		Alert.render();
		event.preventDefault();
		return false;
	}
	else return true;

}

function get(name) {
	if (name = (new RegExp('[?&]' + encodeURIComponent(name) + '=([^&]*)')).exec(location.search))
		return decodeURIComponent(name[1]);
}

function myfunction1() {
	document.getElementById('nm').value = get('nom');
	document.getElementById('prnm').value = get('prenom');
	document.getElementById('email').value = get('email');
	document.getElementById('user').value = get('user');
	document.getElementById('telephone').value = get('telephone');
	document.getElementById('naissance').value = get('naissance');
	if (get('GenreU') == 'Homme') document.getElementById('1').checked = true;
	if (get('GenreU') == 'Femme') document.getElementById('2').checked = true;
	if (get('admin') == 1) document.getElementById('oui').checked = true;
	if (get('admin') == 0) document.getElementById('non').checked = true;
}
 
 
function myfunction2() {
	document.getElementById('typeappartement').value.Selected = get('type');
	document.getElementById('degcit').value = get('DegreCitoyenA');
	document.getElementById('degsec').value = get('DegreSecuriteA');
	document.getElementById('libelle').value = get('LibelleA');
}

function get(name) {
	if (name = (new RegExp('[?&]' + encodeURIComponent(name) + '=([^&]*)')).exec(location.search))
		return decodeURIComponent(name[1]);
}
function customAlert() {
	this.render = function () {
		var winW = window.innerWidth;
		var winH = window.innerHeight;
		var dialogoverlay = document.getElementById('dialogoverlay');
		var dialogbox = document.getElementById('dialogbox');
		var head = document.getElementById('dialogboxhead');
		var body = document.getElementById('dialogboxbody');
		var foot = document.getElementById('dialogboxfoot');
		dialogoverlay.style.display = 'block';
		dialogoverlay.style.height = winH + "px";
		dialogbox.style.left = "34%";
		dialogbox.style.top = "25%";
		dialogbox.style.display = "block";
		head.innerHTML = "Alerte";
		body.innerHTML = '<table><tr><td><img class="alert" src="../../img/alert.png"></td><td>Merci de bien remplir votre \<b>formulaire</b></td></tr></table>';
		foot.innerHTML = "<button href='#' onclick='Alert.cancel()' class='btn'>Ok</button>";
	}
	
		this.alertage = function () {
		var winW = window.innerWidth;
		var winH = window.innerHeight;
		var dialogoverlay = document.getElementById('dialogoverlay');
		var dialogbox = document.getElementById('dialogbox');
		var head = document.getElementById('dialogboxhead');
		var body = document.getElementById('dialogboxbody');
		var foot = document.getElementById('dialogboxfoot');
		dialogoverlay.style.display = 'block';
		dialogoverlay.style.height = winH + "px";
		dialogbox.style.left = "34%";
		dialogbox.style.top = "25%";
		dialogbox.style.display = "block";
		head.innerHTML = "Alerte";
		body.innerHTML = '<table><tr><td><img class="alert" src="../../img/alert.png"></td><td>Votre age est moins que \<b>18 ans</b></td></tr></table>';
		foot.innerHTML = "<button href='#' onclick='Alert.cancel()' class='btn'>Ok</button>";
	}

	this.deletePop = function (id) {
		var winW = window.innerWidth;
		var winH = window.innerHeight;
		var dialogoverlay = document.getElementById('dialogoverlay');
		var dialogbox = document.getElementById('dialogbox');
		var head = document.getElementById('dialogboxhead');
		var body = document.getElementById('dialogboxbody');
		var foot = document.getElementById('dialogboxfoot');
		dialogoverlay.style.display = 'block';
		dialogoverlay.style.height = winH + "px";
		dialogbox.style.left = "34%";
		dialogbox.style.top = "25%";
		dialogbox.style.display = "block";
		head.innerHTML = "Confirmation";
		body.innerHTML = 'Vous êtes sûr de cette suppression ?';
		foot.innerHTML = "<button onclick='Alert.ok(" + id + ")' href='#' class='btn'>OK</button> <button href='#' onclick='Alert.cancel()' class='btn'>Annuler</button>";
	}
	
		this.delete_appart = function (id) {
		var winW = window.innerWidth;
		var winH = window.innerHeight;
		var dialogoverlay = document.getElementById('dialogoverlay');
		var dialogbox = document.getElementById('dialogbox');
		var head = document.getElementById('dialogboxhead');
		var body = document.getElementById('dialogboxbody');
		var foot = document.getElementById('dialogboxfoot');
		dialogoverlay.style.display = 'block';
		dialogoverlay.style.height = winH + "px";
		dialogbox.style.left = "34%";
		dialogbox.style.top = "25%";
		dialogbox.style.display = "block";
		head.innerHTML = "Confirmation";
		body.innerHTML = 'Vous êtes sûr de cette suppression ?';
		foot.innerHTML = "<button onclick='Alert.ok_appart(" + id + ")' href='#' class='btn'>OK</button> <button href='#' onclick='Alert.cancel()' class='btn'>Annuler</button>";
	}
	
		this.delete_piece = function (IdAppart,idPiece) {
		var winW = window.innerWidth;
		var winH = window.innerHeight;
		var dialogoverlay = document.getElementById('dialogoverlay');
		var dialogbox = document.getElementById('dialogbox');
		var head = document.getElementById('dialogboxhead');
		var body = document.getElementById('dialogboxbody');
		var foot = document.getElementById('dialogboxfoot');
		dialogoverlay.style.display = 'block';
		dialogoverlay.style.height = winH + "px";
		dialogbox.style.left = "34%";
		dialogbox.style.top = "25%";
		dialogbox.style.display = "block";
		head.innerHTML = "Confirmation";
		body.innerHTML = 'Vous êtes sûr de cette suppression ?';
		foot.innerHTML = "<button onclick='Alert.ok_piece(" + IdAppart + "," + idPiece + ")' href='#' class='btn'>OK</button> <button href='#' onclick='Alert.cancel()' class='btn'>Annuler</button>";
	}
	
	this.delete_appareil = function (idPiece,idAppareil) {
		var winW = window.innerWidth;
		var winH = window.innerHeight;
		var dialogoverlay = document.getElementById('dialogoverlay');
		var dialogbox = document.getElementById('dialogbox');
		var head = document.getElementById('dialogboxhead');
		var body = document.getElementById('dialogboxbody');
		var foot = document.getElementById('dialogboxfoot');
		dialogoverlay.style.display = 'block';
		dialogoverlay.style.height = winH + "px";
		dialogbox.style.left = "34%";
		dialogbox.style.top = "25%";
		dialogbox.style.display = "block";
		head.innerHTML = "Confirmation";
		body.innerHTML = 'Vous êtes sûr de cette suppression ?';
		foot.innerHTML = "<button onclick='Alert.ok_appareil(" + idPiece + "," + idAppareil + ")' href='#' class='btn'>OK</button> <button href='#' onclick='Alert.cancel()' class='btn'>Annuler</button>";
	}
	

	this.cancel = function () {
		document.getElementById('dialogoverlay').style.display = 'none';
		document.getElementById('dialogbox').style.display = 'none';
		return false;
	}

	this.ok = function (id) {
		location.replace('../../Controleur/utilisateur/supprimer_utilisateur.php?id=' + id);
	}
	
    this.ok_appart = function (id) {
		location.replace('../../Controleur/appartement/supprimer_appartement.php?id=' + id);
	}
	
    this.ok_piece = function (IdAppart,idPiece) {
		location.replace('../../Controleur/piece/supprimer_piece.php?IdAppart=' + IdAppart+"&idPiece="+idPiece);
	}

	this.ok_appareil = function (IdPiece,id) {
		location.replace('../../Controleur/appareil/supprimer_appareil.php?id=' + id);
	}

}
var Alert = new customAlert();