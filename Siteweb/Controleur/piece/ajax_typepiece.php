<?php

function ListeTypePieces(): array {
    include("../../Modele/connexion_bd.php");
    $res = array();

    $req = mysqli_query($bdd, "select * from TypePiece");

    if ($req) {
        while ($row = mysqli_fetch_array($req)) {
            array_push($res, $row);
        }
    }

    mysqli_close($bdd);
    return $res;
}
?>
