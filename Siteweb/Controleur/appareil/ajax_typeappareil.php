<?php

function ListeTypeAppareils(): array {
    include("../../Modele/connexion_bd.php");
    $res = array();

    $req = mysqli_query($bdd, "select * from TypeAppareil");

    if ($req) {
        while ($row = mysqli_fetch_array($req)) {
            array_push($res, $row);
        }
    }

    mysqli_close($bdd);
    return $res;
}


function ListeAppareilsPiece(): array {
    include("../../Modele/connexion_bd.php");
    $res = array();

    $req = mysqli_query($bdd,"select * from piece NATURAL JOIN Appareil NATURAL JOIN TypeAppareil");

    if ($req) {
        while ($row = mysqli_fetch_array($req)) {
            array_push($res, $row);
        }
    }

    mysqli_close($bdd);
    return $res;
}
?>

