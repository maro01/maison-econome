# maison-econome

Ce projet est une application web pour la suivi de consommation des utilisateurs en diverses ressources (Ex: Electricité, gaz, eau,...etc) de leurs maisons, permettant de faire des statistiques des maisons les plus écologiques en terme de consommation.

"Maison-econome" est une application web réalisée par 4 étudiants pour une société de fournitures de services informatiques notamment la conception et creation d’applications web sur mesure. Notre client, la societe anonyme nommée Contrôle de qualité ́de Citoyenneté (CQC SA) Nous soumet son projet. Son rôle est d’encourager les citoyens français à une consommation modérée et ́econome des ressources naturelles. C'est dans cette objectif que cette application web est crée pour  l'objectif de suivre la consommation des citoyens en diverses ressources (Ex: Electricité, gaz, eau,...etc) et l’ ́emission de substances nocives pour l’environnement(Ex: CO2).
